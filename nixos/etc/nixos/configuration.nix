# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  nix.gc = {
    automatic = true;
    options = "--delete-older-than 7d";
    dates = "Mon 12:00:00";
  };


  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.chromium = {
    enableAdobeFlash = true; # for Chromium
    enablePepperFlash = true;
    enablePepperPDF = true;
  };

  nixpkgs.config.packageOverrides = pkgs: (
    let
      idea = import ./packages/idea.nix pkgs;
    in
    {
      idea.idea-community = idea;
      jdk = pkgs.oraclejdk8;
      jre = pkgs.oraclejdk8;
      # sbt = pkgs.sbt.override {
      #   jdk = pkgs.oraclejdk8;
      # };
      xdg_utils = pkgs.xdg_utils.override {
        mimiSupport = true;
      };
    }
  );


  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "w0rkh0rs"; # Define your hostname.
  networking.extraHosts = "127.0.0.1 bsppostgres bsprabbitmq";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Budapest";

  hardware.pulseaudio = {
    enable = true;

  };



  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment = {
    systemPackages = let
      terminal = import ./packages/terminal.nix pkgs;
      launcher = import ./packages/launcher.nix pkgs;
      neovimPackages = import ./packages/neovim/neovimPackages.nix pkgs;
    in
    neovimPackages ++ (with pkgs; [
      chromium
      coreutils
      dtrx
      duplicity
      # exa
      # elixir # TODO: Per shell ?
      # erlang # TODO: Per shell ?
      gitFull
      git-review
      htop
      jq
      launcher
      mc
      networkmanagerapplet
      openssl
      oraclejdk8
      pulseaudioFull
      python35
      python35Packages.neovim
      scala # TODO: Per shell ?
      sbt # TODO: Per shell ?
      slock
      stow
      terminal
      tig
      tldr
      tmux
      tree
      unzip
      wget
      xautolock
      xclip
      xsel
      xorg.xbacklight
      wirelesstools
    ]);
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;
  services.dbus.enable = true;

  services.xserver = {
    enable = true;
    layout = "us,hu";
    displayManager.slim = {
      enable = true;
      defaultUser = "gergelyorosz";
    };
    synaptics = {
      enable = true;
      twoFingerScroll = true;
      tapButtons = false;
    };
    windowManager.awesome = {
      enable = true;
      # TODO: Check if it can be installed this way
      # luaModules = [pkgs.luaPackages.lain];
    };
    windowManager.default = "awesome";
    desktopManager.xfce.enable = true;
    desktopManager.xterm.enable = false;
    desktopManager.default = "none";
  };

  # TODO: Not defined
  # services.xserver.xautolock = {
  #     enable = true;
  #     locker = "slock";
  #   };

  # Enable redshift and set location to Budapest
  services.redshift = {
    enable = true;
    latitude = "47.49801";
    longitude = "19.03991";
  };

  services.ntp = {
    enable = true;
    servers = [
        "kuka.balabit"
        "0.pool.ntp.org"
        "1.pool.ntp.org"
        "2.pool.ntp.org"
      ];
  };

  services.rabbitmq = {
    enable = true;
  };

  services.postgresql = {
      enable = true;
      package = pkgs.postgresql95;
    };


  sound.enable = true;
  # TODO: Not defined
  # sound.mediakeys.enable = true;

  programs.ssh.startAgent = true;

  users.defaultUserShell = "${pkgs.zsh}/bin/zsh";
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.gergelyorosz = {
    isNormalUser = true;
    uid = 1000;
    initialPassword = "titkos";
    extraGroups = [ "networkmanager" "wheel" "docker" ];
    description = "Gergely Orosz";
  };

  # Setup fonts
  fonts = {
    enableCoreFonts = true;
    enableFontDir = true;
    enableGhostscriptFonts = false;
    fonts = with pkgs; [
      inconsolata
      corefonts
      ubuntu_font_family
      hack-font
      fira-code
      fira
      fira-mono
      terminus_font
    ];
  };

  virtualisation.docker = {
    enable = true;
    enableOnBoot = false;
    liveRestore = false;
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.03";

}
