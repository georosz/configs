{ pkgs, ... }:

let
  attachTmux = ''
    #!/bin/sh
    if [ $(tmux ls | wc -l) -gt 0 ]; then
      tmux attach -d -t $(tmux ls | fzf --reverse -e --prompt=">" --header="Choose a tmux session to attach" | cut -f1 -d:)
    else
      tmux
    fi
  '';
  tTerm = ''
    #!/bin/sh
    if [ -z $@ ]; then
      exec termite -e attach-tmux
    else 
      export SHELL=tmux
      exec termite "$@"
    fi
  '';
  installPhaseScript = ''
    mkdir -p $terminfo/share
    mv $out/share/terminfo $terminfo/share/terminfo

    mkdir -p $out/nix-support
    echo "$terminfo" >> $out/nix-support/propagated-user-env-packages
    
    echo '${attachTmux}' > $out/bin/attach-tmux
    chmod +x $out/bin/attach-tmux
    
    echo '${tTerm}' > $out/bin/tterm
    chmod +x $out/bin/tterm
  '';


in
  pkgs.stdenv.lib.overrideDerivation pkgs.termite (attrs: {
    postInstall = installPhaseScript;
  })

