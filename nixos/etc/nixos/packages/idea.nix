pkgs:
let
  ldPath = pkgs.stdenv.lib.makeLibraryPath [ pkgs.libsecret ];
in
  with pkgs;
  pkgs.stdenv.lib.overrideDerivation pkgs.idea.idea-community (attrs: {
    jdk = pkgs.oraclejdk;

    installPhase = builtins.concatStringsSep "\n" [
      attrs.installPhase
      ''
      rm -rf $out/bin/idea-community
      makeWrapper "$out/$name/bin/idea.sh" $out/bin/idea-community \
        --prefix PATH : "$out/libexec/${attrs.name}:${pkgs.stdenv.lib.makeBinPath [ jdk coreutils gnugrep which git ]}" \
        --set JDK_HOME "$jdk" \
        --set IDEA_JDK "$jdk" \
        --set ANDROID_JAVA_HOME "$jdk" \
        --set LD_LIBRARY_PATH "${ldPath}" \
        --set JAVA_HOME "$jdk"
      ''
    ];
  })
