{ pkgs }:

let
  # this is the vimrc.nix from above
  vimrc = pkgs.callPackage ./vimrc.nix {};

  # and the plugins.nix from above
  plugins = pkgs.callPackage ./plugins.nix {};
in
{
  customRC = vimrc;
  vam = {
    knownPlugins = pkgs.vimPlugins // plugins;

    pluginDictionaries = [
      # from pkgs.vimPlugins
      { name = "neosnippet"; }
      { name = "neosnippet-snippets"; }
      { name = "deoplete-nvim"; }
      { name = "vim-polyglot"; }
      { name = "neomake"; }
      { name = "alchemist-vim"; }
      { name = "vim-multiple-cursors"; }
      { name = "rainbow_parentheses"; }
      { name = "vim-scala"; }
      { name = "elm-vim"; }
      { name = "The_NERD_Commenter"; }
      { name = "undotree"; }
      { name = "vim-nix"; }
      { name = "fzf-vim"; }
      { name = "fugitive"; }

      # from our own plugin package set
      { name = "vim-trailing-whitespace"; }
      { name = "delimitMate"; }
      { name = "landscapeTheme"; }
      { name = "phoenix"; }
      { name = "vim-projectionist"; }

    ];
  };
}
