if &compatible
  set nocompatible               " Be iMproved
endif

set mouse=""
set tabstop=2
set softtabstop=2
set expandtab
set shiftwidth=2
set number
set encoding=utf-8
set hlsearch
set incsearch
set ignorecase
set smartcase
set cursorline
set cursorcolumn
set title
set autowrite
set nowritebackup
set ruler
set eol
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:.
set list
set colorcolumn=119
set background=dark
" set rtp+=~/.nix-profile
set encoding=utf-8


map <CR> :noh<CR>

" Required:
filetype plugin indent on

let g:alchemist_tag_disable = 1

" Run Neomake when I save any buffer
augroup localneomake
  autocmd! BufWritePost * Neomake
augroup END
" let g:python3_host_prog = '/home/gergelyorosz/.nix-profile/bin/python3.6'
" Don't tell me to use smartquotes in markdown ok?
let g:neomake_markdown_enabled_makers = []
let g:neomake_elixir_enabled_makers = ['mix', 'credo']


syntax enable
set background=dark
colorscheme landscape

" Run Neomake when I save any buffer
augroup localneomake
  autocmd! BufWritePost * Neomake
augroup END

" Don't tell me to use smartquotes in markdown ok?
let g:neomake_markdown_enabled_makers = []

let g:deoplete#enable_at_startup = 1
let g:mapleader=','

" NerdCommenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1
" [[B]Commits] Customize the options used by 'git log':
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
" [Tags] Command to generate tags file
let g:fzf_tags_command = 'ctags -R'

nnoremap <silent> <C-E> :tabnext<CR>
nnoremap <silent> <C-D> :tabprevious<CR>
nnoremap <silent> <C-F> :tabnew<CR>
nnoremap <silent> <C-W> :tabclose<CR>
nnoremap <silent> <C-O> :tabedit<CR>
nnoremap <leader>p :FZF<CR>
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==
nnoremap <leader>u :UndotreeToggle<CR>
nnoremap <leader>gv :GV<CR>
nnoremap <leader>ls :SyntasticReset<CR>

if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif

au BufNewFile,BufRead COMMIT_EDITMSG setlocal spell
