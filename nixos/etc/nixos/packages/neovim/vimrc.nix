{ stdenv, writeText }:

let
  neovimInit = builtins.readFile ./vimrc/init.vim;
in
  ''
    ${neovimInit}
  ''