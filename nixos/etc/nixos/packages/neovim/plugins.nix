{ pkgs, fetchgit }:

let
  buildVimPlugin = pkgs.vimUtils.buildVimPluginFrom2Nix;
in {
  "vim-trailing-whitespace" = buildVimPlugin {
    name = "vim-trailing-whitespace";
    src = fetchgit {
      url = "https://github.com/bronson/vim-trailing-whitespace";
      rev = "d4ad27de051848e544e360482bdf076b154de0c1";
      sha256 = "1xrx9c8d4qwf38jc44a9szw195m8rfri9bsdcj701p9s9ral0l31";
    };
    dependencies = [];
  };

  "delimitMate" = buildVimPlugin {
    name = "delimitMate";
    src = fetchgit {
      url = "https://github.com/Raimondi/delimitMate";
      rev = "728b57a6564c1d2bdfb9b9e0f2f8c5ba3d7e0c5c";
      sha256 = "0fskm9gz81dk8arcidrm71mv72a7isng1clssqkqn5wnygbiimsn";
    };
    dependencies = [];
  };

  "landscapeTheme" = buildVimPlugin {
    name = "landscapeTheme";
    src = fetchgit {
      url = "https://github.com/itchyny/landscape.vim";
      rev = "3156bf3e148803b8e9132d840f75ccca8302b845";
      sha256 = "1hr3m97mfbvj06pciah86r9l8hbhsj1p064npn7gpz3fisql6gjx";
    };
    dependencies = [];
  };

  "phoenix" = buildVimPlugin {
    name = "phoenix";
    src = fetchgit {
      url = "https://github.com/c-brenn/phoenix.vim";
      rev = "723be122f14a97246023190cee5f2f473ee8ec7b";
      sha256 = "1lz3zkink5m83byhjyp50b7bxj9b93j7f9agnfzkfbqf9qvaa5yc";
    };
    dependencies = [];
  };

  "vim-projectionist" = buildVimPlugin {
    name = "vim-projectionist";
    src = fetchgit {
      url = "https://github.com/tpope/vim-projectionist";
      rev = "88e84056e2b3bb74356c13789a935a66b121780e";
      sha256 = "17j8326lw48vjy0fidappfmcwcars088j9bfji6rcjkdd58vff1p";
    };
    dependencies = [];
  };
}
