{pkgs, ...}:

let
  rofiRun = ''
    #!/bin/sh

    rofi -show run - color-enabled: true -color-window "#393939, #393939, #268bd2" \
    -color-normal "#393939, #ffffff, #393939, #268bd2, #ffffff" \
    -color-active "#393939, #268bd2, #393939, #268bd2, #205171" \
    -color-urgent "#393939, #f3843d, #393939, #268bd2, #ffc39c" \
    -font "Fora Code Retina 16"
  '';
  installSteps = ''
    echo '${rofiRun}' > $out/bin/rofi-run
    chmod +x $out/bin/rofi-run
  '';
in
  pkgs.stdenv.lib.overrideDerivation pkgs.rofi (attrs: {
    postInstall = installSteps;
  })
  
