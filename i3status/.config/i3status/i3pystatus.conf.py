from i3pystatus import Status

status = Status(
    logfile='/home/gergelyorosz/.local/var/i3pystatus.log',
)

# Displays clock like this:
# Tue 30 Jul 11:59:46 PM KW31
status.register("clock",
                format="%a %-d %b %X",)

# Displays and changes current keyboard layout
status.register("xkblayout",
                layouts=['us', 'us colemak' , 'hu'])


# This would also display a desktop notification (via D-Bus) if the percentage
# goes below 5 percent while discharging. The block will also color RED.
# If you don't have a desktop notification demon yet, take a look at dunst:
#   http://www.knopwob.org/dunst/
status.register("battery",
                format="{status}|{percentage:.2f}% {remaining:%E%hh:%Mm}",
                alert=True,
                alert_percentage=5,
                status={"DIS": "↓",
                        "CHR": "↑",
                        "FULL": "=",
                        },
                )

vpn_status_command = 'bash -c \'nmcli con show Balabit | grep "GENERAL.STATE:\s*activated"\''
vpn_up_command = 'nmcli con up id Balabit passwd-file /home/gergelyorosz/.dotfiles/cert/vpn_pwd'
vpn_down_command= 'nmcli con down id Balabit'
status.register("openvpn",
                vpn_name="Balabit",
                status_command=vpn_status_command,
                vpn_up_command=vpn_up_command,
                vpn_down_command=vpn_down_command)


# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: the network module requires PyPI package netifaces
status.register("network",
                interface="eno1",
                format_up="{v4cidr}",)

# Note: requires both netifaces and basiciw (for essid and quality)
status.register("network",
                interface="wlp2s0",
                format_up="{essid} {quality:03.0f}%",)



# TODO: Add spotify

# status.register('spotify')

# Shows pulseaudio default sink volume
#
# Note: requires libpulseaudio from PyPI
status.register("pulseaudio",
                format="♪{volume}",)

status.run()
