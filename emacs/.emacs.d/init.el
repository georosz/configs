;;; init.el --- emacs init script
;;; Commentary:
;;; Code:
(setq gc-cons-threshold 50000000)

(add-hook 'emacs-startup-hook 'my/set-gc-threshold)
(defun my/set-gc-threshold ()
  "Reset `gc-cons-threshold' to its default value."
  (setq gc-cons-threshold 800000))

(setq user-full-name "Gergely Orosz"
      user-mail-address "gergely.orosz@balabit.com")

(add-to-list 'load-path "~/.emacs.d/libs")
    (load "visws.el")
    (load "redspace.el")

(server-start)

;; Please set your themes directory to 'custom-theme-load-path
(add-to-list 'custom-theme-load-path
             (file-name-as-directory "/home/gergelyorosz/.emacs.d/themes"))

;; load your favorite theme
(load-theme 'oceanic t t)
(enable-theme 'oceanic)

;; global variables
(defvar show-paren-delay)
(defvar use-package-always-ensure)
(set-frame-font "Fira Code 11" nil t)
(setq
 inhibit-startup-screen t
 create-lockfiles nil
 make-backup-files nil
 column-number-mode 1
 size-indication-mode t
 scroll-error-top-bottom t
 show-paren-delay 0.5
 use-package-always-ensure t
 sentence-end-double-space nil)
;;(setq-default default-directory "C:/Users/Lucas/")
;; buffer local variables
(setq-default
 indent-tabs-mode nil
 tab-width 2
 c-basic-offset 2
 default-directory "/home/gergelyorosz/Devel")

;; modes
(electric-indent-mode 0)
(add-hook 'after-init-hook 'global-company-mode)
(add-hook 'after-init-hook 'projectile-mode)

;; global keybindings
(global-unset-key (kbd "C-z"))

;; the blinking cursor is nothing, but an annoyance
(blink-cursor-mode -1)

;; Change "yes or no" to "y or n"
(fset 'yes-or-no-p 'y-or-n-p)
(unless window-system
  (tool-bar-mode -1))
(unless window-system
  (menu-bar-mode -1))
;; Line numbers
(defvar linum-format)
(global-linum-mode 1)
(setq linum-format "%d ")
;; Newline at end of file
(setq require-final-newline t)

;; Make sure it uses UTF-8 across the board
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(setq default-file-name-coding-system 'utf-8)

(defvar browse-url-generic-program)
(setq browse-url-browser-function 'browse-url-generic
          browse-url-generic-program "opera-beta")

(add-hook 'before-save-hook 'redspace-check-buffer)


(defun move-line-up ()
  "Move one line up."
  (interactive)
  (transpose-lines 1)
  (forward-line -2))

(defun move-line-down ()
  "Move one line down."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1))


;; (define-key input-decode-map "\e\eOA" [(meta up)])
;; (define-key input-decode-map "\e\eOB" [(meta down)])
(global-set-key [(meta j)] 'move-line-up)
(global-set-key [(meta k)] 'move-line-down)
(global-set-key (kbd "M-o")  'mode-line-other-buffer)

;; #### PACKAGES ####

;; the package manager
(require 'package)
(setq
 package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                    ("org" . "http://orgmode.org/elpa/")
                    ("melpa" . "http://melpa.org/packages/")
                    ("melpa-stable" . "http://stable.melpa.org/packages/"))
 package-archive-priorities '(("melpa-stable" . 1)))

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; (use-package todotxt-mode
;;   :ensure t
;;   :config
;;   (setq todotxt-default-file (expand-file-name "/home/gergelyorosz/Dropbox/Apps/Simpletask App Folder/todo/todo.txt"))
;;   (define-key global-map "\C-co" 'todotxt-open-file)
;;   (define-key global-map "\C-ct" 'todotxt-add-todo)
;;   (add-to-list 'auto-mode-alist '("\\todo.txt\\'" . todotxt-mode)))

(use-package evil
  :ensure t
  :demand
  :config
  (evil-mode 1))

(use-package ensime
  :ensure t
  :pin melpa-stable
  :config
  (add-hook 'scala-mode-hook 'ensime-scala-mode-hook))

(use-package elixir-mode
  :ensure t
  :pin melpa-stable
  :config
  (add-to-list 'auto-mode-alist '("\\.elixir2\\'" . elixir-mode)))

(use-package alchemist
  :ensure t)

(use-package sbt-mode
  :pin melpa)

(use-package scala-mode
  :pin melpa
  :config
  (add-to-list 'auto-mode-alist '("\\.scala\\'" . scala-mode)))

(use-package projectile
  :ensure t
  :config
  (setq projectile-switch-project-action #'helm-projectile-find-file))

(use-package helm-projectile
  :ensure t
  :bind (("C-x f" . helm-projectile-find-file)))

(use-package helm
  :diminish helm-mode
  :init
  (progn
    (require 'helm-config)
    (setq helm-candidate-number-limit 100)
    ;; From https://gist.github.com/antifuchs/9238468
    (setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
          helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
          helm-yas-display-key-on-candidate t
          helm-quick-update t
          helm-M-x-requires-pattern nil
          helm-ff-skip-boring-files t)
    (helm-mode))
  :bind (("C-c h" . helm-mini)
         ("C-h a" . helm-apropos)
         ("C-x C-b" . helm-buffers-list)
         ("C-x b" . helm-buffers-list)
         ("M-y" . helm-show-kill-ring)
         ("M-x" . helm-M-x)
         ("C-x c o" . helm-occur)
         ("C-x c s" . helm-swoop)
         ("C-x c y" . helm-yas-complete)
         ("C-x c Y" . helm-yas-create-snippet-on-region)
         ("C-x c SPC" . helm-all-mark-rings)))
(ido-mode -1) ;; Turn off ido mode in case I enabled it accidentally

(use-package helm-git-grep
  :ensure t)

(use-package smartparens
  :ensure t
  :config
  (require 'smartparens-config)
  (smartparens-global-mode 1))

(use-package elpy
  :ensure t)

(use-package auto-complete
   :ensure t
   :config
   (ac-config-default)
   (global-auto-complete-mode 1))

(use-package magit
  :ensure t)

(use-package yasnippet
  :ensure t)

(use-package helm-c-yasnippet
  :ensure t)

(use-package haskell-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.hs\\'" . haskell-mode)))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package elm-mode
  :ensure t)

(use-package helm-fuzzy-find
  :ensure t)

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package multiple-cursors
  :ensure t
  :bind (("C-x l" . mc/edit-lines)
         ("C-x w" . mc/mark-next-like-this)))

(use-package salt-mode
  :ensure t)

(use-package groovy-mode
  :pin melpa
  :ensure t)

(use-package org
  :ensure t
  :bind (("C-c a" . org-agenda)))

(use-package evil-leader
  :ensure t)

(use-package evil-org
  :ensure t
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode))

(defun autocompile nil
  "Compile itself if ~/.emacs."
  (interactive)
  (require 'bytecomp)
  (let ((dotemacs (file-truename user-init-file)))
    (if (string= (buffer-file-name) (file-chase-links dotemacs))
      (byte-compile-file dotemacs))))


(use-package slack
  :commands (slack-start)
  :init
  (setq slack-buffer-emojify t) ;; if you want to enable emoji, default nil
  (setq slack-prefer-current-team t)
  :config
  (slack-register-team
   :name "balabit"
   :default t
   :client-id "3272405144.213680974113"
   :client-secret "27f56e5b59d9aff4635b50ce239c5075"
   :token "xoxp-3272405144-70105555606-215109456822-28a1087f52b6f9d84aa0fccad07eca7b"
   :subscribed-channels '(blindspotter pam-integration)))

(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'notifier))

(use-package powershell
  :ensure t
  :mode (("\\.ps1\\'" . powershell-mode)))

(use-package tabbar
  :ensure t
  :init (tabbar-mode))

(use-package git-gutter
  :ensure t
  :init (global-git-gutter-mode +1))

(use-package evil-mc
  :ensure t
  :init (global-evil-mc-mode 1))

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'after-save-hook 'autocompile)
(add-hook 'scala-mode-hook
          '(lambda ()
             (yas/minor-mode-on)))

(setq org-agenda-files (list "~/oroszgergely840705@gmail.com/Dokumentumok/Notes/notes.org"
                             "~/oroszgergely840705@gmail.com/Dokumentumok/Notes/balabit.org"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(groovy-indent-offset 2)
 '(package-selected-packages
   (quote
    (evil-mc git-gutter tabbar powershell slack evil-leader leader evil-org helm-c-yasnippet elixir-mode ensime evil use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;;; (provide 'init)
;;; init.el ends here
