if &compatible
  set nocompatible               " Be iMproved
endif

set mouse=""
set tabstop=2
set softtabstop=2
set expandtab
set shiftwidth=2
set number
set encoding=utf-8
set hlsearch
set incsearch
set ignorecase
set smartcase
set cursorline
set cursorcolumn
set title
set autowrite
set nowritebackup
set ruler
set eol
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:.
set list
set colorcolumn=119
set background=dark
set rtp+=~/.fzf
set encoding=utf-8


map <CR> :noh<CR>

" ---- Dein settings ---- 
" Required:
set runtimepath+=/home/gergelyorosz/.config/nvim/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('/home/gergelyorosz/.config/nvim/dein')
  call dein#begin('/home/gergelyorosz/.config/nvim/dein')
  " Required:

  " Let dein manage dein
  " Required:
  call dein#add('Shougo/dein.vim')

  " Add or remove your plugins here:
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('sheerun/vim-polyglot')
  call dein#add('itchyny/landscape.vim')
  call dein#add('neomake/neomake')
  call dein#add('c-brenn/phoenix.vim')
  call dein#add('tpope/vim-projectionist')
  call dein#add('slashmili/alchemist.vim')
  call dein#add('junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all'})
  call dein#add('junegunn/fzf.vim')
  call dein#add('terryma/vim-multiple-cursors')
  call dein#add('kien/rainbow_parentheses.vim')
  call dein#add('Raimondi/delimitMate')
  call dein#add('derekwyatt/vim-scala')
  call dein#add('elmcast/elm-vim')
  call dein#add('scrooloose/nerdcommenter')
  call dein#add('mbbill/undotree')
  call dein#add('herrbischoff/cobalt2.vim')
  call dein#add('LnL7/vim-nix')
  call dein#add('sickill/vim-monokai')
  call dein#add('mhartington/oceanic-next')

  " You can specify revision/branch/tag.
  call dein#add('Shougo/vimshell', { 'rev': '3787e5' })

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif



let g:alchemist_tag_disable = 1

" Run Neomake when I save any buffer
augroup localneomake
  autocmd! BufWritePost * Neomake
augroup END
let g:python3_host_prog = '/usr/bin/python'
" Don't tell me to use smartquotes in markdown ok?
let g:neomake_markdown_enabled_makers = []
let g:neomake_elixir_enabled_makers = ['mix', 'credo']

if (has("termguicolors"))
 set termguicolors
endif

syntax enable
set background=dark
colorscheme OceanicNext
" Run Neomake when I save any buffer
augroup localneomake
  autocmd! BufWritePost * Neomake
augroup END
 " Don't tell me to use smartquotes in markdown ok?
let g:neomake_markdown_enabled_makers = []
let g:deoplete#enable_at_startup = 1
let g:mapleader=','

" NerdCommenter
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'
" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1
" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }
" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1
" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1
" [[B]Commits] Customize the options used by 'git log':
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
" [Tags] Command to generate tags file
let g:fzf_tags_command = 'ctags -R'

nnoremap <silent> <C-E> :tabnext<CR>
nnoremap <silent> <C-D> :tabprevious<CR>
nnoremap <silent> <C-F> :tabnew<CR>
nnoremap <silent> <C-W> :tabclose<CR>
nnoremap <silent> <C-O> :tabedit<CR>
nnoremap <leader>p :FZF<CR>
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==
nnoremap <leader>u :UndotreeToggle<CR>
nnoremap <leader>gv :GV<CR>
nnoremap <leader>ls :SyntasticReset<CR>

if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
endif

au BufNewFile,BufRead COMMIT_EDITMSG setlocal spell
