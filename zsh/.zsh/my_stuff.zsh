# All the stuff I use
. $ZSH_DOTFILES_DIR/z/z.sh

source $ZSH_DOTFILES_DIR/git.zsh

# Variables
DIRSTACKSIZE=8

setopt autopushd pushdminus pushdsilent pushdtohome

function pretty_print { echo -e "\e[1;32m$1\e[0m"; }
function echo_red { echo -e "\e[91m\e[1m$1\e[0m"; }
function echo_green { echo -e "\e[92m\e[1m$1\e[0m"; }
function echo_header { echo -e "\e[1;4m$1\e[0m"; }

function copy_with_rsync {
    source_dir=$1
    destination_dir=$2
    rsync -az --progress "$source_dir" "$destination_dir"
}

# List all files, long format, colorized, permissions in octal
function la {
    ls -l  "$@" | awk '
    {
      k=0;
      for (i=0;i<=8;i++)
        k+=((substr($1,i+2,1)~/[rwx]/) *2^(8-i));
      if (k)
        printf("%0o ",k);
      printf(" %9s  %3s %2s %5s  %6s  %s %s %s\n", $3, $6, $7, $8, $5, $9,$10, $11);
    }'
}

# Copy w/ progress
function cp_p {
  rsync -WavP --human-readable --progress "$1" "$2"
}

function turn_on_second_monitor {
    if [ `xrandr | grep -c ' connected '` -eq 2 ]; then # dual-monitor
        if [ `xrandr | grep DP1-1 | grep -c ' connected '` -eq 1 ]; then
            xrandr --output eDP1 --auto --primary --output DP1-1 --auto --right-of eDP1 --mode 1920x1080
        fi
    else
        xrandr --output eDP1 --auto --primary --output DP1-1 --off
    fi

}

function touch_and_vim {
    if [[ -z "$1" ]]; then
        echo 'Need a file name'
    fi
    touch $1
    nvim $1
}

function to_iso {
    local epoch=$1
    date -d @$epoch
}

function to_epoch {
    local d=${1:-`date +%Y-%m-%d`}
    echo $d
    date --date=$d +%s
}

function rpmcontent {
   RPM=`realpath $1`
   rm -rf target/rpm/content
   mkdir -p target/rpm/content
   pushd target/rpm/content
   rpm2cpio $RPM | cpio -V -uid
   tree -I '*.jar'
   echo "jars emitted"
   popd
}

function check_sha256 {
    local check_sum="$1"
    local file=$2
    sha256_digest=`sha256sum ${file} | awk '{print $1}'`
    echo_header "Comparing sha256 sums"
    if [ $check_sum = "$sha256_digest" ]; then
        echo_green "OK"
    else
        echo_red "Checksum mismatch"
    fi
}


# Folders
Devel=/home/gergelyorosz/Devel
SP=/home/gergelyorosz/Devel/Python/shiny_product
PyhtonProjects=/home/gergelyorosz/Devel/Python

# add local .bin folder
export PATH=$PATH:~/.local/bin

# Global aliases
alias -g GE='georosz' GER='gergelyorosz'

# Aliases
alias ..='cd ..'
alias ~='cd ~'
alias cls='clear'
alias cr='copy_with_rsync'
alias d='dtrx --one here'
alias dh='dirs -v'
alias e='emacs -nw'
alias g='git'
alias gmf='git-magic-fixup'
alias hup='hup'
alias lf='ls -lah --group-directories-first --color'
alias .l='exa -lah --group-directories-first'
alias lock='xscreensaver-command -l'
alias monitor='turn_on_second_monitor eDP1 DP1-1'
alias monitor_off='xranrd --output DP1-1 --off'
alias mux='tmuxinator'
alias now='date +%s'
alias restart='sudo shutdown -r'
alias s='subl -w'
alias sshmedia='ssh georosz@192.168.0.104'
alias sus='sudo systemctl suspend'
alias ts='task'
alias tv='touch_and_vim'
alias zr='. ~/.zshrc'
alias ze='s ~/.dotfiles/zsh/.zshrc'
alias zem='s ~/.dotfiles/zsh/.zsh/my_stuff.zsh'
alias v='nvim'
alias vim='nvim'
alias webserver='python3 -m http.server 8000'
alias work='mux start work'
alias xo='xdg-open'

# docker aliases
alias dim='docker images'
alias dl='docker logs'
alias dlf='docker logs -f'
alias docker_clean='docker_clean'
alias docker_start='sudo systemctl start docker'
alias dps='docker ps -a'
alias dps='docker ps'
alias dpsa='docker ps -a'
alias dr='docker'
alias drm='docker rm -f'
alias drma='docker_remove_containers'
alias drmi='docker rmi -f'
alias drmia='docker_remove_images'
alias drn='docker_remove_none'
alias dsrm='docker_stop_and_remove'
alias dsw='docker_start_and_logs'
alias dvl='docker volume ls'
alias dvrm='docker volume rm'
alias dvrma='docker_remove_volumes'
