function prompt() {
    local message=$1

    while true; do
        read -p "$message ?" yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer yes or no.";
        esac
    done
}

function log_error() {
    local RED='\033[0;31m'
    local NC='\033[0m'
    local msg=$1
    echo "${RED} ${msg} ${NC}" >&2
}

function force_push_branch() {
    local LOCAL_BRANCH_NAME=`git rev-parse --abbrev-ref HEAD` 2>/dev/null

    if [ -z "$LOCAL_BRANCH_NAME" ]; then
        log_error "No git repository found !!!!"
        return 1
    else
        if [ "$LOCAL_BRANCH_NAME" = "master" ]; then
            log_error "No force push to master !!!!!"
            return 1
        else
            echo "Force pushing to: $LOCAL_BRANCH_NAME"
            git push --force-with-lease origin $LOCAL_BRANCH_NAME
        fi
    fi
}

function gshn() {
  ([ -z "$1" ] || [ $(($1)) -lt 0 ]) && echo 'Invalid integer!' && return
  git show HEAD@{$1}
}

function gbrr() {
    local GBRR_DEFAULT_COUNT=10
    local COUNT=${1-$GBRR_DEFAULT_COUNT}

      IFS=$'\r\n' BRANCHES=($(
        git reflog | \
        sed -n 's/.*checkout: moving from .* to \(.*\)/\1/p' | \
        perl -ne 'print unless $a{$_}++' | \
        head -n $COUNT
      ))

      for ((i = 0; i < ${#BRANCHES[@]}; i++)); do
        echo "$i) ${BRANCHES[$i]}"
      done

      read -p "Switch to which branch? "
      if [[ $REPLY != "" ]] && [[ ${BRANCHES[$REPLY]} != "" ]]; then
        echo
        git checkout ${BRANCHES[$REPLY]}
      else
        echo Aborted.
      fi
}

function ggo() {
  $EDITOR $(git grep --name-only "$@")
}

function ggio() {
  $EDITOR $(git grep -i --name-only "$@")
}

function rename_git_branch() {
    # unify-scripted-account-baselines-BSP-891
    old_branch=$1
    new_branch=$2
    echo '### Setting current branch to: ' $old_branch
    git checkout "$old_branch"
    echo '### Renaming branch from: ' "$old_branch" ' to: ' "$new_branch"
    git branch -m "$old_branch" "$new_branch"
    echo '### Deleting remote branch with name: ' "$old_branch"
    git push origin :"$old_branch"
    echo '### Unset old upstream ###'
    git branch --unset-upstream
    echo '### Setting upstream to: ' "$new_branch"
    git push --set-upstream origin "$new_branch"
}

# Rebase from previous branch
function grbp() {
  br="$(git reflog | sed -n 's/.*checkout: moving from .* to \(.*\)/\1/p' | sed "2q;d")"
  git rebase $br
}

function gputu() {
  if [ -z "$1" ]; then
    br="$(git rev-parse --abbrev-ref HEAD)"
  else
    br="$1"
  fi
  git push -u origin $br
}

function topcmds() {
  [ ! -z $1 ] && n="$1" || n="10"
  history | awk '{a[$2 " " $3]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head -n $n
}

########## Aliases ##########
alias ga='git add'
alias gaa='git add -A'
alias gamd='git commit --amend --no-edit'
alias gbr="git branch --color"
alias gbrb="git checkout -"
alias gbrc='git rev-parse --abbrev-ref HEAD'
alias gbrp='git reflog | sed -n "s/.*checkout: moving from .* to \(.*\)/\1/p" | sed "2q;d"'
alias gcl='git clone'
alias gcf='git diff --name-only'
alias gclme='git branch --merged ${1-master} | grep -v " ${1-master}$" | xargs -r git branch -d; '
alias gcm='git commit --signoff --verbose'
alias gco='git checkout'
alias gcob='git checkout -b'
alias gcp='git cherry-pick'
alias gdiscardall='git checkout -- .'
alias gdrb='git push origin --delete'
alias gfp='force_push_branch'
alias gget='git pull'
alias ggi='git grep -i'
alias ggno='git grep --name-only'
alias ggr='git grep'
alias glast="tig log -1 HEAD"
alias glog="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'"
alias gput='git push'
alias grb='git rebase'
alias grba='git rebase --abort'
alias grbc='git rebase --continue'
alias grbi='git rebase --interactive'
alias grbr='git branch -D'
alias grom='git rebase --interactive origin/master'
alias grbs='git rebase --skip'
alias grh='git reset HEAD'
alias gsa='git stash apply'
alias gsave='git add -A && git commit -s -m "feat(SAVE): SAVEPOINT"'
alias gsd='git stash drop'
alias gsh='git show'
alias gsl='git stash list'
alias gsp='git stash pop'
alias gss='git stash save'
alias gssh='git stash show -p'
alias gst='git status -sb'
alias gsth='git stash'
alias gundo='git reset HEAD~1 --mixed'
alias gunstage='git reset HEAD --'
alias gvis='gitk'
alias gwip='git commit -sam "feat(WIP): WIP"'
alias gwipe='git add -A && git commit -qsm "feat(WIPE): WIPE SAVEPOINT" && git reset HEAD~1 --hard'
