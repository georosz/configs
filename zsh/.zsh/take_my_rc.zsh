IP=""
USER=""

function sshrc() {
}

function _ssh() {

    ssh $USER@$IP
}

function _copy_id() {
    ssh-keygen -R $IP
    ssh-keyscan $IP >> ~/.ssh/known_hosts
    ssh-copy-id $USER@$IP
}


function main() {
    echo "Main"
    USER="$1"
    IP="$2"
    echo "$USER@$IP"
}

alias sshrc='main'
