#/usr/bin/env zsh

docker_clean() {
    echo 'Removing containers'
    docker rm -f $(docker ps -a -q)
    echo 'Removing images'
    docker rmi -f $(docker images -q)
    echo "Removing local volumes"
    docker volume ls -q | xargs docker volume rm
}

docker_remove_none() {
    docker images | grep '^<none>' | awk '{print $3}' | xargs docker rmi -f
}

docker_stop_and_remove() {
    docker stop "$1"
    docker rm -f "$1"
}

docker_start_and_logs() {
    docker start "$1"
    docker logs -f "$1"
}

docker_remove_containers() {
    docker ps -aq | xargs docker rm -f
}

docker_remove_images() {
    docker images -q | xargs docker rmi -f
}

docker_remove_volumes() {
    docker volume ls -a | xargs docker volume rm
}

docker_remove_none_tags() {
    docker images | awk '{print $2, $3}' | grep '^<none>' | awk '{print $2}' | xargs docker rmi
}
